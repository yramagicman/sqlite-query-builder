import Database, { Statement } from "better-sqlite3";

type Value = string | number | boolean;

type UpdateWhere = {
    column: string,
    value: Value,
};

type Constraint = 'PRIMARY KEY' | 'UNIQUE' | 'NULL' | 'NOT NULL' | 'CHECK';

type DataType = 'NULL' | 'INTEGER' | 'REAL' | 'TEXT' | 'BLOB';

type Column = {
    column: string,
    constraint?: Constraint,
    dataType: DataType,
    default?: Value,
    autoIncrement: boolean
};

type RunResult = { changes: number, lastInsertRowid: number };

type Where = {
    column: string,
    value: Value,
    comparison?: string,
};

type AlterAction = 'RENAME TO' | 'RENAME COLUMN' | 'ADD COLUMN' | 'DROP COLUMN';

export class Q {
    query: string;
    error: boolean;
    errorMessage: string;
    parameters: Array<Value>;
    db: any;

    constructor(database: string) {
        this.db = new Database(database);
        this.query = '';
        this.parameters = [];
        this.error = false;
        this.errorMessage = '';
    }

    select(table: string, columns: Array<string>): Q {
        this.query = `SELECT ${columns.join(', ')} FROM ${table}`;
        return this;
    }

    selectDistinct(table: string, columns: Array<string>): Q {
        this.query = `SELECT DISTINCT ${columns.join(', ')} FROM ${table}`;
        return this;
    }

    join(table: string, tableAColumn: string, tableBColumn: string): Q {
        return this.ensureQueryStartsWithSelectThen('calls to join() must follow calls to select()', () => {
            this.query += ` JOIN ${table} ON ${tableAColumn} = ${tableBColumn}`;
            return this;
        });
    }

    where(columns: Array<Where>): Q {
        return this.ensureQueryStartsWithSelectThen('calls to where() must follow calls to select()', () => {
            const params = this.mapColumns(columns, ' WHERE', 'AND');
            this.query += params.join(' ');
            const values = columns.map(c => c.value);
            this.parameters = [...this.parameters, ...values];
            return this;

        });
    }

    orWhere(columns: Array<Where>): Q {
        return this.ensureQueryStartsWithSelectThen('calls to where() must follow calls to select()', () => {
            const params = this.mapColumns(columns, ' OR', 'OR');
            this.query += params.join(' ');
            const values = columns.map(c => c.value);
            this.parameters = [...this.parameters, ...values];
            return this;

        });
    }

    update(table: string, where: UpdateWhere): Q {
        let statement = `UPDATE ${table} `;
        this.parameters = [...this.parameters,  where.value]
        statement += 'SET ' + where.column + ' ' + '=' + ` ?`;
        this.query = statement;
        return this;
    }
    delete(table: string, where: Where): Q {
        let statement = `DELETE FROM ${table} `;
        this.parameters = [...this.parameters,  where.value]
        statement += 'WHERE ' + where.column + ' ' + '=' + ` ?`;
        this.query = statement;
        return this;
    }

    insert<T>(table: string, columns: T): Q {
        let params: string[] = new Array(Object.values(columns as Object).length);
        params = params.fill('?', 0, Object.values(columns as Object).length);

        this.query = 'INSERT into ' + table;
        this.query += ' (' + Object.keys(columns as Object).map(c => "'" + c).join("',") + "')";
        this.query += ' values (' + params.join(', ') + ')';
        this.parameters = [...this.parameters, ...Object.values(columns as Object)];
        return this;
    }

    createTable(table: string, tableDefinition: Array<Column>): Q {
        this.query = `CREATE TABLE IF NOT EXISTS ${table}`;
        const columns = tableDefinition.map(column => {
            return `${column.column} ${column.dataType} ${column.constraint ?? ''} ${column.autoIncrement === true ? 'AUTOINCREMENT' : ''}`.replace(',', '');
        });
        this.query += `( ${columns.join(', ')} )`;
        return this;
    }

    addColumn(table: string, columnToAdd: Column): Q {
        this.alterTable(table, 'ADD COLUMN', columnToAdd);
        return this;
    }

    renameColumn(table: string, from: string, to: string): Q {
        this.alterTable(table, 'RENAME COLUMN', { 'from': from, 'to': to });
        return this;
    }

    dropColumn(table: string, target: string): Q {
        this.alterTable(table, 'DROP COLUMN', target);
        return this;
    }

    renameTable(table: string, target: string): Q {
        this.alterTable(table, 'RENAME TO', target);
        return this;
    }

    run(): RunResult {
        return this.runQuery((a: Statement) => a.run(this.parameters)) as RunResult;
    }

    all<T>(): Array<T> {
        return this.runQuery((a: Statement) => a.all(this.parameters)) as Array<T>;
    }

    first<T>():  T  {
        return this.runQuery((a: Statement) => a.get(this.parameters)) as T;
    }

    printSql() {
        console.log(this.query);
        console.log(this.parameters);
    }

    private runQuery<T>(callback: CallableFunction): T | Array<T> | boolean {
        if (this.error) {
            console.error(this.errorMessage)
            this.reset()
            return false;
        }
        const statement = this.db.prepare(this.query);
        const result = callback(statement);

        this.reset()
        return result;

    }

    private reset() {
        this.parameters = [];
        this.query = '';
        this.error = false;
        this.errorMessage = '';
    }

    private alterTable(table: string, action: AlterAction, target: any): Q {
        this.query = `ALTER TABLE ${table} `;
        this.query += action;
        switch (action) {
            case 'ADD COLUMN':
                this.query += ` ${target.column} ${target.dataType} ${target.constraint ?? ''} ${target.autoIncrement === true ? 'AUTOINCREMENT' : ''} `.replace(',', '');
                break;
            case 'RENAME COLUMN':
                this.query += ` ${target.from} TO ${target.to}`;
                break;

            case 'RENAME TO':
                this.query += ` ${target}`;
                break;
            case 'DROP COLUMN':
                this.query += ` ${target}`;
                break;
        };
        return this;
    }

    private ensureQueryStartsWithSelectThen(message: string, callback: CallableFunction): Q {
        if (this.query.substring(0, 6) !== 'SELECT' && this.query.substring(0, 6) !== 'UPDATE') {
            this.errorMessage = message
            this.error = true;
            return this;
        }
        return callback();
    }

    private mapColumns(columns: Array<Where>, firstIndex: string, restIndex: string) {
        return columns.map((key: Where, index: number) => {

            let andWhere = firstIndex
            if (index > 0) andWhere = restIndex;
            if (key.comparison) {
                return andWhere + ' ' + key.column + ' ' + key.comparison + ` ?`
            }
            return andWhere + ' ' + key.column + ' ' + '=' + ` ?`
        });
    }
}
