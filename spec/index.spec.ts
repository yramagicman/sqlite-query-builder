import { Q } from '../src/index';
describe("A suite", function () {
    it("Produces a reasonable select statement", function () {
        const q = new Q(':memory:');
        const result = q.select('steve', ['one', 'two', 'three']);
        expect(result.query).toEqual('SELECT one, two, three FROM steve');
    });

    it("Produces a reasonable select distinct statement", function () {
        const q = new Q(':memory:');
        const result = q.selectDistinct('steve', ['one', 'two', 'three']);
        expect(result.query).toEqual('SELECT DISTINCT one, two, three FROM steve');
    });

    it("Produces a reasonable join statement", function () {
        const q = new Q(':memory:');
        const result = q.select('steve', ['*']).join('mary', 'steve.id', 'mary.steve_id');
        expect(result.query).toEqual('SELECT * FROM steve JOIN mary ON steve.id = mary.steve_id');
    });

    it("produces a reasonable WHERE statement with one comparison", function () {
        const q = new Q(':memory:');
        const result = q.select('steve', ['one', 'two', 'three'])
            .where([
                { column: 'a', value: 'one', },
                { column: 'b', value: 'two', },
                { column: 'c', value: 'three', },
            ]);
        expect(result.query).toEqual("SELECT one, two, three FROM steve WHERE a = ? AND b = ? AND c = ?");
        expect(result.parameters).toEqual(['one', 'two', 'three']);
    });

    it("produces a reasonable OR WHERE statement with one comparison", function () {
        const q = new Q(':memory:');
        const result = q.select('steve', ['one', 'two', 'three'])
            .where([{ column: 'z', value: 'seven' }])
            .orWhere([{ column: 'a', value: 'one' }, { column: 'b', value: 'two' }, { column: 'c', value: 'three' }]);
        expect(result.query).toEqual("SELECT one, two, three FROM steve WHERE z = ? OR a = ? OR b = ? OR c = ?");
        expect(result.parameters).toEqual(['seven', 'one', 'two', 'three']);
    });

    it("produces a reasonable WHERE statement with multiple comparisons", function () {
        const q = new Q(':memory:');
        const result = q.select('steve', ['one', 'two', 'three'])
            .where([
                { column: 'a', value: 'one', comparison: '=' },
                { column: 'b', value: 'two', comparison: '<' },
                { column: 'c', value: 'three', comparison: '>' },
            ]);

        expect(result.query).toEqual("SELECT one, two, three FROM steve WHERE a = ? AND b < ? AND c > ?");
        expect(result.parameters).toEqual(['one', 'two', 'three']);
    });

    it("produces a reasonable OR WHERE statement with multiple comparisons", function () {
        const q = new Q(':memory:');

        const result = q.select('steve', ['one', 'two', 'three']).where([{ column: 'z', value: 'seven' }]).orWhere([
            { column: 'a', value: 'one', comparison: '=' },
            { column: 'b', value: 'two', comparison: '<' },
            { column: 'c', value: 'three', comparison: '>' },
        ]);
        expect(result.query).toEqual("SELECT one, two, three FROM steve WHERE z = ? OR a = ? OR b < ? OR c > ?");
        expect(result.parameters).toEqual(['seven', 'one', 'two', 'three']);
    });

    it("fails to produce a reasonable WHERE statement when called without SELECT", function () {
        const q = new Q(':memory:');
        const result = q.where([
            { column: 'a', value: 'one', comparison: '=' },
            { column: 'b', value: 'two', comparison: '<' },
            { column: 'c', value: 'three', comparison: '>' },
        ]);
        expect(result.query).toEqual("");
        expect(result.error).toEqual(true);
        expect(result.errorMessage).toEqual('calls to where() must follow calls to select()');
    });

    it("produces a reasonable Update statement without given comparison", function () {
        const q = new Q(':memory:');
        const result = q.update('steve', { column: 'a', value: 'one' }).where([{column: 'a', value: 'stuff'}]) ;
        expect(result.query).toEqual("UPDATE steve SET a = ? WHERE a = ?");
        expect(result.parameters).toEqual(['one', 'stuff']);
    });

    it("produces a reasonable Update statement with given comparison", function () {
        const q = new Q(':memory:');
        const result = q.update('steve', { column: 'a', value: 'one' }).where([{column: 'a', comparison: '<', value: 'stuff'} ]);
        expect(result.query).toEqual("UPDATE steve SET a = ? WHERE a < ?");
        expect(result.parameters).toEqual(['one', 'stuff']);
    });

    it("produces a reasonable Insert statement", function () {
        const q = new Q(':memory:');
        const result = q.insert('steve', {
            'a': 'one',
            'b': 'two',
            'c': 'three'
        });
        expect(result.query).toEqual("INSERT into steve ('a','b','c') values (?, ?, ?)");
        expect(result.parameters).toEqual(['one', 'two', 'three']);
    });

    it("produces a reasonable delete statement", function () {
        const q = new Q(':memory:');
        const result = q.delete('steve', {column: 'a', value: 'b'});
        expect(result.query).toEqual("DELETE FROM steve WHERE a = ?");
        expect(result.parameters).toEqual(['b']);
    });
    it("produces a reasonable create table statement", function () {
        const q = new Q(':memory:');
        const result = q.createTable('steve', [
            { column: 'id', constraint: 'PRIMARY KEY', dataType: 'INTEGER', autoIncrement: true },
            { column: 'username', constraint: 'NOT NULL', dataType: 'TEXT', autoIncrement: false },
            { column: 'given_name', constraint: 'NULL', dataType: 'TEXT', autoIncrement: false },
        ]);
        expect(result.query).toEqual("CREATE TABLE IF NOT EXISTS steve( id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL , given_name TEXT NULL  )");
    });

    it("produces a reasonable alter table statement to add a column", function () {
        const q = new Q(':memory:');
        const result = q.addColumn(
            'steve',
            { autoIncrement: false, column: 'smith', dataType: 'TEXT', constraint: 'NULL' }
        );
        expect(result.query).toEqual("ALTER TABLE steve ADD COLUMN smith TEXT NULL  ");
    });

    it("produces a reasonable alter table statement to rename a column", function () {
        const q = new Q(':memory:');
        const result = q.renameColumn('steve', 'a', 'b');
        expect(result.query).toEqual("ALTER TABLE steve RENAME COLUMN a TO b");
    });

    it("produces a reasonable alter table statement to drop a column", function () {
        const q = new Q(':memory:');
        const result = q.dropColumn('steve', 'a');
        expect(result.query).toEqual("ALTER TABLE steve DROP COLUMN a");
    });

    it("produces a reasonable alter table statement to rename a table", function () {
        const q = new Q(':memory:');
        const result = q.renameTable('steve', 'a');
        expect(result.query).toEqual("ALTER TABLE steve RENAME TO a");
    });

    it("creates a database and queries", function () {
        const q = new Q(':memory:');
        q.createTable('steve', [
            { column: 'id', constraint: 'PRIMARY KEY', dataType: 'INTEGER', autoIncrement: true },
            { column: 'username', constraint: 'NOT NULL', dataType: 'TEXT', autoIncrement: false },
            { column: 'given_name', constraint: 'NULL', dataType: 'TEXT', autoIncrement: false },
            { column: 'stupid_name', constraint: 'NULL', dataType: 'TEXT', autoIncrement: false },
        ]).run();

        q.createTable('mary', [
            { column: 'id', constraint: 'PRIMARY KEY', dataType: 'INTEGER', autoIncrement: true },
            { column: 'steve_id', constraint: 'NULL', dataType: 'INTEGER', autoIncrement: false },
            { column: 'mary_name', constraint: 'NOT NULL', dataType: 'TEXT', autoIncrement: false },
        ]).run();

        q.addColumn('steve', { column: 'surname', constraint: 'NULL', dataType: 'TEXT', autoIncrement: false }).run();

        q.insert('steve', { 'username': 'two', 'given_name': 'three', 'surname': 'four' }).run();
        q.insert('steve', { 'username': 'help', 'given_name': 'me', 'surname': 'here' }).run();
        q.insert('steve', { 'username': 'help', 'given_name': 'me', 'surname': 'here', 'stupid_name': 'stupid' }).run();
        q.insert('mary', { 'mary_name': 'here', 'steve_id': 2 }).run();
        const a = q.insert('mary', { 'mary_name': 'bob', 'steve_id': 3, }).run();
        expect(a.changes).toEqual(1);
        type Result = {
            username: string,
            given_name: string,
            surname: string,
            stupid_name?: string,
            steve_id?: number,
            mary_name?: string,
        };

        const result = q.select('steve', ['username', 'given_name', 'surname']).all<Result>();
        const where = q.select('steve', ['username', 'given_name', 'surname'])
            .where([{ column: 'username', value: 'help' }, { column: 'given_name', value: 'me' }])
            .first<Result>();

        const orwhere = q.select('steve', ['username', 'given_name', 'surname', 'stupid_name'])
            .where([{ column: 'username', value: 'help' }, { column: 'given_name', value: 'me' }])
            .orWhere([{ column: 'stupid_name', value: 'stupid' }])
            .all<Result>();
        expect(result).toEqual([{ username: 'two', given_name: 'three', surname: 'four' }, { username: 'help', given_name: 'me', surname: 'here' }, { username: 'help', given_name: 'me', surname: 'here' }])
        expect(where.given_name).toEqual('me');
        expect(orwhere.find(a => a.stupid_name !== null)?.stupid_name).toEqual('stupid');
        expect(orwhere.find(a => a.stupid_name === null)?.given_name).toEqual('me');
        const join = q.select('steve', ['*']).join('mary', 'steve.id', 'mary.steve_id').all<Result>();
        expect(join[0]?.mary_name).toEqual('here');

        const joinWhere = q.select('steve', ['*']).join('mary', 'steve.id', 'mary.steve_id')
            .where([{ column: 'steve.id', value: 2 }])
            .all<Result>();
        expect(joinWhere[0]?.mary_name).toEqual('here');
        expect(joinWhere.find(a => a.mary_name === 'bob')).toBeUndefined();

        const forgotSelect = q.join('mary', 'steve.id', 'mary.steve_id').first() as Q;
        expect(forgotSelect).toBeFalse();
        const forgotSelectWhere = q.where([{ column: 'mary_name', value: 'bob' }]).first() as Q;
        expect(forgotSelectWhere).toBeFalse();

        const distinct = q.selectDistinct('steve', [
            'username',
            'given_name',
            'stupid_name',
            'surname'
        ]).all<Result>();
        expect(distinct.find(a => a.stupid_name === 'stupid')).toEqual({
            username: 'help',
            given_name: 'me',
            stupid_name: 'stupid',
            surname: 'here'
        });


        q.update('steve', {column: 'stupid_name', value: 'not stupid'}).where([{column: 'username', value: 'help'}]).run();

        expect(q.select('steve', [

            'username',
            'given_name',
            'stupid_name',
            'surname',

        ]).where([{column: 'username', value: 'help'}]).first<Result>()).toEqual({
            username: 'help',
            given_name: 'me',
            stupid_name: 'not stupid',
            surname: 'here'
        });
    });
});
